import React, { useContext } from 'react';
import Context from "../../Context"
import "./index.css"

const FaderButton = (props) => {
    const { idx, setIdx, imgs, changeImage } = useContext(Context)
    if (idx < 0) setIdx(idx + imgs.length)
    return (
        <div className={`arrow ${props.direction === -1 ? "left" : "right"}`} onClick={() => changeImage((idx + props.direction) % imgs.length)}></div>
    );
};

export default FaderButton;