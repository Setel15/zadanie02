import React, { useContext, useState } from 'react';
import Context from "../../Context"
import FaderViews from "../FaderViews"
import FaderButton from "../FaderButton"
import FaderDot from '../FaderDot';
import "./index.css"

const Fader = () => {
    const { imgs } = useContext(Context)
    return (
        < div className="row" >
            <FaderButton direction={-1} />
            <div className="column">
                <FaderViews />
                <div className="row">{imgs.map((img, i) => <FaderDot key={i} id={i} />)}</div>
            </div>
            <FaderButton direction={1} />
        </div >
    );
};

export default Fader;