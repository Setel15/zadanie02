import React, { useContext } from 'react';
import context from "../../Context"
const FaderViews = (props) => {
    const { idx, imgs, opacity } = useContext(context)
    return (
        <div>
            <img src={`https://cdn.pixabay.com/photo/${imgs[idx]}.jpg`} alt="view" height="300px" width="450px" style={{ opacity: opacity }} />
        </div>
    );
};

export default FaderViews;