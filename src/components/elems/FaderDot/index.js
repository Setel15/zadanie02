import React, { useContext } from 'react';
import Context from "../../Context"
import "./index.css"
const FaderDot = (props) => {
    const { idx, changeImage } = useContext(Context)
    return (
        <div className={"dot"} onClick={() => changeImage(props.id)}>
            <div style={{ backgroundColor: idx === props.id ? "black" : "transparent" }}></div>
        </div>
    );
};

export default FaderDot;