import React, { useState } from 'react'

const Context = React.createContext()
const Provider = (props) => {
    const [idx, setIdx] = useState(0)
    const [opacity, setOpacity] = useState(1)
    const changeImage = (newId) => {
        setOpacity(0)
        setTimeout(() => { setIdx(newId); setOpacity(1) }, 1000)
    }
    const imgs = ["2017/07/07/12/31/lime-2481346__340", "2019/08/04/14/58/morning-4383986_960_720", "2018/06/25/14/27/background-image-3497025_960_720", "2018/05/11/15/18/background-3390802_960_720", "2017/08/30/01/05/milky-way-2695569_960_720"]
    return (
        <Context.Provider value={{ idx, setIdx, opacity, changeImage, imgs }}>
            {props.children}
        </Context.Provider>
    )
}

export default Context
export { Provider }