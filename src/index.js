import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import { Provider } from "./components/Context"
import App from './components/elems/Fader';
import * as serviceWorker from './serviceWorker';

ReactDOM.render(
  <React.StrictMode>
    <Provider>
      <App />
    </Provider>
  </React.StrictMode>,
  document.getElementById('root')
);
serviceWorker.unregister();